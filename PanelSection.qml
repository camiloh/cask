import QtQuick 2.13
import QtQml 2.14
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13
import QtGraphicalEffects 1.0

import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui

Item
{
    id: control
    default property alias content : _content.data
    property alias cards : _cards.contentChildren
    property int popWidth : Math.min(_cask.avaliableWidth, Math.max(300, width))
    property alias popHeight : popup.height

    property alias backgroundColor: _rec.color
    property alias spacing: _content.spacing

    property bool collapsed : false
    property int position : ToolBar.Footer

    Layout.minimumWidth: implicitWidth
    Layout.margins: Maui.Style.space.medium
    Layout.fillWidth: false

    implicitWidth: collapsed ? 0 : _content.implicitWidth
    implicitHeight: collapsed ? 0 : Maui.Style.toolBarHeightAlt


    Rectangle
    {
        id: _rec
        anchors.fill: parent

        radius: Maui.Style.radiusV
        opacity: 0.8
        color: Kirigami.Theme.backgroundColor
    }

    RowLayout
    {
        id: _content
        anchors.fill: parent
        spacing: 0
        clip: true
    }

    Item
    {
        id: popup
        property int suggestedHeight
        y: control.position === ToolBar.Footer ? 0 - (height + Maui.Style.space.medium) : control.height + Maui.Style.space.medium
//x: Math.min(0, control.width - width)
        Binding on suggestedHeight {
        value:  handler.active ? (control.position === ToolBar.Footer ? 0 - handler.centroid.position.y : handler.centroid.position.y) : popup.height
        restoreMode: Binding.RestoreBinding
        }

        height: suggestedHeight > 200 ? Math.min (_cask.avaliableHeight, _cardsList.contentHeight) : Math.min(suggestedHeight, handler.active ?  500 : 0)
        width: control.popWidth - x

        Rectangle
        {
            id: popupBg
            anchors.fill: parent
            color: Kirigami.Theme.backgroundColor
            radius: Maui.Style.radiusV
//            border.color: Qt.tint(Kirigami.Theme.textColor, Qt.rgba(Kirigami.Theme.backgroundColor.r, Kirigami.Theme.backgroundColor.g, Kirigami.Theme.backgroundColor.b, 0.7))
            opacity: 0.2
        }

        Container
        {
            id: _cards
            anchors.fill: parent
            clip: true

            contentItem: ListView
            {
                id:_cardsList
                spacing: Maui.Style.space.medium
                model: _cards.contentModel
                snapMode: ListView.SnapOneItem
                orientation: ListView.Vertical
            }
        }


        layer.enabled: true
        layer.effect: OpacityMask
        {
            maskSource: Item
            {
                width: popup.width
                height: popup.height

                Rectangle
                {
                    anchors.fill: parent
                    radius: popupBg.radius
                }
            }
        }
    }

    DragHandler
    {
        id: handler
        target: null
    }

    Connections
    {
        target: _cask

        onDesktopPressed:
        {
            control.close()
            mouse.accepted = false
        }
    }


    function close()
    {
        console.log("Close it")
        popup.suggestedHeight = 0

    }

    function open()
    {
        console.log("Pop it up")
        popup.suggestedHeight= 500
    }
}

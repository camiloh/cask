import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui
import org.kde.plasma.private.kicker 0.1 as Kicker

Maui.ApplicationWindow
{
    id: root
    Maui.App.enableCSD: true
    headBar.visible: false

    property int isWide : width > 800

    enum FormFactor
    {
        Phone,
        Phablet,
        Tablet,
        Laptop,
        Desktop
    }

    //    color: "#00000000"

    //        visibility: Window.FullScreen


    Cask
    {
        id: _cask
        anchors.fill: parent
        backgroundImage: "qrc:/calamares_wallpaper.jpg"

        bottomPanel.children: isWide ? [taskManagerBar, statusBar] :  [taskManagerBar]
        topPanel.children: isWide ? [] :  [statusBar]

       }

    readonly property QtObject statusBar : StatusBar {}

    property QtObject taskManagerBar: TaskBar {}

}

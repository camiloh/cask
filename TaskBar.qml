import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui

PanelSection
{
    id: control
 Layout.fillWidth: true
    backgroundColor: "transparent"
    spacing: Maui.Style.space.medium
    ToolButton
    {
        icon.name: "view-list-icons"
        onClicked: {
           control.open()
        }
    }

    ToolButton
    {
        icon.name: "application-menu"
    }

    Row
    {
        Layout.fillWidth: true
        spacing: Maui.Style.space.big

        Repeater
        {
            model: ["index", "vvave", "nota", "buho", "pix"]

            Kirigami.Icon
            {
                source:modelData
                height: Maui.Style.iconSizes.big
                width: height
            }
        }
    }

   cards: PanelCard
   {
       width: parent.width
       height: 500
       padding: 0

       Maui.Page
           {
              anchors.fill: parent
              anchors.margins: 1
               headBar.visible: true
               margins: 10
               flickable: _launcherGrid.flickable
               headBar.middleContent: Maui.TextField
               {
                   Layout.fillWidth: true
                   placeholderText: qsTr("Search for apps and files...")
               }

               background: Rectangle
               {
                   color: Kirigami.Theme.backgroundColor
                   opacity: 0.7
                   radius: Maui.Style.radiusV
               }

               headBar.leftContent: Maui.ToolActions
               {
                   expanded: root.isWide
                   autoExclusive: true

                   Action
                   {
                       icon.name: "view-list-icons"
                       text: qsTr("Grid")
                   }

                   Action
                   {
                       icon.name: "view-list-details"
                       text: qsTr("List")
                   }
               }

               Maui.GridView
               {
                   id: _launcherGrid
                   anchors.fill: parent
                   model: Maui.KDE.appsList()
                   adaptContent: true

                   itemSize: 100

                   verticalScrollBarPolicy:  Qt.ScrollBarAlwaysOff
                   delegate: Maui.ItemDelegate
                   {
                       width: _launcherGrid.cellWidth
                       height: _launcherGrid.cellHeight

                       Maui.GridItemTemplate
                       {
                           height: _launcherGrid.itemSize
                           width: _launcherGrid.itemSize
                           anchors.centerIn: parent
                           iconSource:  modelData.icon
                           iconSizeHint: 48
                           label1.text: modelData.label
                       }
                   }
               }
           }
   }
}

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui

ItemDelegate
{
id: control

default property alias content: _layout.data
implicitHeight: _layout.implicitHeight
width: popWidth

background: Rectangle
{
    color: Kirigami.Theme.backgroundColor
    border.color: Qt.tint(Kirigami.Theme.textColor, Qt.rgba(Kirigami.Theme.backgroundColor.r, Kirigami.Theme.backgroundColor.g, Kirigami.Theme.backgroundColor.b, 0.7))
    radius: Maui.Style.radiusV
}


contentItem: Column
{
    id: _layout

}

}

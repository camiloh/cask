import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui

Panel
{
    id: control
    property int position : root.isWide ? ToolBar.Footer : ToolBar.Header

    Layout.fillWidth: true

    PanelSection
    {
        visible: root.isWide
        position: control.position
        ToolButton
        {
            icon.name: !_trayBar.collapsed ? "go-previous" : "go-next"
            icon.color: "#fff"
            onClicked: _trayBar.collapsed = !_trayBar.collapsed
        }
    }

    PanelSection
    {
        id: _trayBar
        visible: root.isWide
        position: control.position
        ToolButton
        {
            icon.name: "call-incoming"
        }

        ToolButton
        {
            icon.name: "input-keyboard"
        }

        ToolButton
        {
            icon.name: "anchor"
        }
    }

    PanelSection
    {
        Layout.fillWidth: !root.isWide
        position: control.position
        ToolButton
        {
            icon.name: "camera-ready"
        }

        ToolButton
        {
            icon.name: "mic-ready"
        }

        ToolButton
        {
            icon.name: "headphones"
        }

        ToolButton
        {
            icon.name: "audio-volume-medium"
        }

        Item
        {
            Layout.fillWidth: !root.isWide
        }

        ToolButton
        {
            display: ToolButton.TextBesideIcon
            icon.name: "battery-080"
            text: "80%"
        }

        ToolButton
        {
            icon.name: "network-wireless"
        }

        Label
        {
            color: "white"
            Layout.fillHeight: true
            text: new Date().toTimeString()
            verticalAlignment: Qt.AlignVCenter
            horizontalAlignment: Qt.AlignHCenter
            elide: Text.ElideMiddle
            wrapMode: Text.NoWrap
            Layout.preferredWidth: implicitWidth + Maui.Style.space.medium


        }

        ToolButton
        {
            icon.name: "notifications"
        }

        cards: [
            PanelCard
            {
                width: parent.width
                height: _tooglesGrid.contentHeight

                Maui.GridView
                {
                    id:_tooglesGrid
                    anchors.fill: parent
                    itemSize: 80
                    verticalScrollBarPolicy:  Qt.ScrollBarAlwaysOff


                    model: ["network-bluetooth", "input-keyboard-virtual", "rotation-allowed","webcam", "accessories-calculator",  "settings-configure"]
                    delegate: ItemDelegate
                    {
                        height: _tooglesGrid.cellHeight
                        width: _tooglesGrid.cellWidth
                        background: null
                        Kirigami.Icon
                        {
                            source: modelData
                            anchors.centerIn: parent
                            height: Maui.Style.iconSizes.medium
                            width: height
                        }
                    }
                }

            },

            PanelCard
            {
                width: parent.width
                height: 100

                Maui.ListItemTemplate
                {
                    anchors.fill: parent
                    imageSource: "qrc:/Nina Simone_I Put A Spell On You_1965.jpg"
                    imageSizeHint: 90
                    spacing: Maui.Style.space.medium

                    label1.text: "That's Him Over There"
                    label2.text: "Nina Simone"
                }
            },

            PanelCard
            {
              width: parent.width
              height: _nof.contentHeight

              ListView
              {
                  id: _nof
                  anchors.fill: parent
                  model: 10
                  spacing: Maui.Style.space.medium
                  delegate: Maui.ItemDelegate
                  {
                      width: parent.width
                      height: 80

                      Maui.ListItemTemplate
                      {
                          anchors.fill: parent
                          iconSource: "documentinfo"
                          label1.text: "Notification Title"
                          label2.text: "Blach some infor about the notification"
                          iconSizeHint: Maui.Style.iconSizes.medium
                          spacing: Maui.Style.space.medium
                      }

                      onClicked: _nof.model--

                  }
              }

            },

            PanelCard
            {
                width: parent.width
                height: 100

                Label
                {
                    text: qsTr("Volume")
                }

                Slider
                {
                    width: parent.width
                }

                Label
                {
                    text: qsTr("Brigtness")
                }


                Slider
                {
                    width: parent.width
                }
            },

            PanelCard
            {
                width: parent.width
                height: 64

                RowLayout
                {
                    anchors.fill: parent
                    Repeater
                    {
                        model: ["system-reboot", "system-shutdown", "system-lock-screen","webcam", "system-suspend"]
                        delegate:  Item
                        {
                            Layout.fillHeight: true
                            Layout.fillWidth: true

                            Kirigami.Icon
                            {
                                anchors.centerIn: parent
                                source: modelData
                                height: Maui.Style.iconSizes.medium
                                width: height
                            }
                        }
                    }
                }
            }

        ]


    }
}

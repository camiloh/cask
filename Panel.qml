import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import org.kde.mauikit 1.2 as Maui

RowLayout
{
    id: control
    visible: children.length > 0
    height: visible ? implicitHeight : 0
}
